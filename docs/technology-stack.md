# [SDKMAN!](https://sdkman.io/)

The Software Development Kit Manager

## Why?

Don't know any alternative. Mostly "just works", despite a few quirks:
* Not XDG Directory Spec compliant
* Doesn't have a simple "list installed" command

## Project usage
Manage (Java) tooling versions, namely Java and Gradle.

# [Gradle](https://gradle.org/)

Build Tool

## Why?

Only have experience with Gradle and Maven...
and I prefer Gradle by **a lot**.

Also, I believe it is the "default" for Android development.

# [Kotlin](https://kotlinlang.org/)

Programming Language

## Why?

Besides being one of my favorite languages,
it has first class support in the main target platform(s):
* Android (Phone)
* Android Wear (Watch)

# [Markdown](https://www.markdownguide.org/)

## Why?

Most experience with. I feel like it serves it purpose very well.
